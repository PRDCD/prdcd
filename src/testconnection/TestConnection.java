/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testconnection;

import java.beans.Statement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.PreparedStatement;

import java.sql.SQLException;

import testRecords.Category;
import testRecords.CategoryList;

/**
 *
 * @author Alec
 */
public class TestConnection {

    /**
     * @param args the command line arguments
     */
    //jdbc:oracle:thin:@tom:1522:ORCL12C
    private String dbName;
    private Connection conn;
    private PreparedStatement pstmt;
    private ResultSet rset;
    
    public TestConnection(){
        dbName = "PRCDC";
    }
    
    public void createConnection() throws SQLException, ClassNotFoundException {
        //Thiis loads JDBC driver and sets up connection
        Class.forName("sun.jdbcc.odbc.Jdbc0dbcDriver");
        conn = DriverManager.getConnection("jdbcc:odbc:PRDCD");
        
    }
    
    public void closeConnection() throws SQLException, ClassNotFoundException {
        conn.close();
    }
    
    public CategoryList allCategory() throws SQLException, ClassNotFoundException {
        String queryString;
        CategoryList categories;
        
        queryString = "Select * FROM CATEGORY";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();
        
        categories = new CategoryList();
        while (rset.next()) {
            categories.addCategory(new Category(rset.getString(1)));
    }
        
        return categories ;
    }
    
}
